package ProjetTest2.ProjetTest2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class postgresJDBC {
	//URL de connexion
	 private String url = "jdbc:postgresql://192.168.11.159:5432/stalmist";
	//Nom du user
	private String user = "stalmist";
	//Mot de passe de l'utilisateur
	private String passwd = "stalmist";
	//Objet Connection
	private Connection connect;
	
	private static postgresJDBC instance;
	
	//Constructeur privé
	private postgresJDBC() {
		try {
			connect = DriverManager.getConnection(url, user, passwd);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static postgresJDBC getInstance() {
		if (instance == null)
			instance = new postgresJDBC();
		return instance;
	}

	public Connection getConnect() {
		return connect;
	}
	
}
