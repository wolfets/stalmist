package ProjetTest2.ProjetTest2;

import java.awt.Point;
import java.util.ArrayList;


public abstract class Joueur {
	private int nbrBateau = 5;
	private ArrayList<Bateau> mesBateaux = new ArrayList<Bateau>();
	private ArrayList<Point> mesTirs = new ArrayList<Point>();//seulement ennemie ?
	private String name="";
	
	public ArrayList<Point> getMesTirs() {
		return mesTirs;
	}
	public void setMesTirs(ArrayList<Point> mesTirs) {
		this.mesTirs = mesTirs;
	}
	public int getNbrBateau() {
		return nbrBateau;
	}
	public void setNbrBateau(int nbrBateau) {
		this.nbrBateau = nbrBateau;
	}
	public ArrayList<Bateau> getMesBateaux() {
		return mesBateaux;
	}
	public void setMesBateaux(ArrayList<Bateau> mesBateaux) {
		this.mesBateaux = mesBateaux;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public abstract Boolean createBateau(ArrayList<Point> p);
	public Boolean isDead(){
		int nbrDead=0;
		for(int i=0;i<getMesBateaux().size();i++){
			if(getMesBateaux().get(i).isDead())nbrDead++;
		}
		if(nbrDead==getMesBateaux().size())return true;
		else return false;
	}
	public Boolean intersect(Bateau b){
		Bateau cur;
		Point pt;
		Boolean retour = false;
		for(int i =0;i <getMesBateaux().size();i++){
			cur= getMesBateaux().get(i);
			for(int j=0;j< cur.getMesPosition().size();j++){
				pt=cur.getMesPosition().get(j);
				for(int k=0; k< b.getMesPosition().size();k++){
					if((pt.x == b.getMesPosition().get(k).x)&&(pt.y == b.getMesPosition().get(k).y))retour = true;
				}
			}
		}
		return retour;
	}
	public abstract void initialize();
	public Boolean isHit(Point p){
		Boolean retour =false;
		mesTirs.add(p);
		for(int i=0;i<mesBateaux.size();i++){
			if(mesBateaux.get(i).isHit(p))retour=true;
		}
		return retour;
	}
	public void afficherPosition(){
		for(int i=0; i<getNbrBateau();i++){
			this.getMesBateaux().get(i).affichePosition();
		}
	}
	
	
}